const ejs = require('ejs')
const formatters = require('./formatters')
const barcode = require('./barcode')
const path = require('path')
const moment = require('moment')

var banks = null

var hashString = function (string) {
  var hash = 0
  var i
  var chr
  var len

  if (string.length == 0) return hash
  for (i = 0, len = string.length; i < len; i++) {
    chr = string.charCodeAt(i)
    hash = ((hash << 5) - hash) + chr
    hash |= 0 // Convert to 32bit integer
  }
  return hash
}

var Boleto = function (options) {
  if (!options) {
    throw 'No options provided initializing Boleto.'
  }

  this.bank = banks[options['banco']]
  if (!this.bank) {
    throw 'Invalid bank.'
  }

  if (!options['data_emissao']) {
    options['data_emissao'] = moment().utc()
  } else {
    options['data_emissao'] = moment(moment(options['data_emissao']).utc().format('YYYY-MM-DD'))
  }

  if (!options['data_vencimento']) {
    options['data_vencimento'] = moment().utc().add('5', 'days')
  } else {
    options['data_vencimento'] = moment(moment(options['data_vencimento']).utc().format('YYYY-MM-DD'))
  }

  for (var key in options) {
    this[key] = options[key]
  }

  this['pagador'] = formatters.htmlString(this['pagador'])
  this['instrucoes'] = formatters.htmlString(this['instrucoes'])

  if (this['descontos'] !== undefined) {
    if (this['descontos'].length > 0) {
      this['descontos'].forEach((discount) => {
        this['instrucoes'] += formatters.htmlString("\n Conceder desconto de ");
        if (discount.value) this['instrucoes'] += formatters.formatAmount(discount.value);
        if (discount.percentage) this['instrucoes'] += String(discount.percentage).replace('.', ',') + "%";
        this['instrucoes'] += " se pago até " + moment(discount.date).utc().format('DD/MM/YYYY');
      })
    }
  }

  if (this['multa']) {
    if (this['multa'].tipo !== 3 && this['multa'].tipo !== null) {
      this['instrucoes'] += formatters.htmlString("\n Cobrar multa de ");
      if (this['multa'].valor) this['instrucoes'] += formatters.formatAmount(this['multa'].valor);
      if (this['multa'].porcentagem) this['instrucoes'] += String(this['multa'].porcentagem).replace('.', ',') + "%";
      this['instrucoes'] += " após data de vencimento " + (moment(this['data_vencimento']).utc().format('DD/MM/YYYY'));
    }
  }

  if (this['juros']) {
    if (this['juros'].tipo !== 5 && this['juros'].tipo !== null) {
      this['instrucoes'] += formatters.htmlString("\n Cobrar juros de ");
      if (this['juros'].tipo === 1) {
        if (this['juros'].valor) this['instrucoes'] += "R$ " + formatters.formatIntWithComma(this['juros'].valor, 2);
        this['instrucoes'] += " ao dia após vencimento " + (moment(this['data_vencimento']).utc().format('DD/MM/YYYY'));
      }
      if (this['juros'].tipo === 2) {
        if (this['juros'].porcentagem) this['instrucoes'] += String(this['juros'].porcentagem).replace('.', ',') + "%";
        this['instrucoes'] += " ao dia após vencimento " + (moment(this['data_vencimento']).utc().format('DD/MM/YYYY'));
      }
      if (this['juros'].tipo === 3) {
        if (this['juros'].porcentagem) this['instrucoes'] += String(this['juros'].porcentagem).replace('.', ',') + "%";
        this['instrucoes'] += " ao mês após vencimento " + (moment(this['data_vencimento']).utc().format('DD/MM/YYYY'));
      }
    }
  }

  if (!this['local_de_pagamento']) {
    this['local_de_pagamento'] = 'Até o vencimento, preferencialmente no Banco ' + formatters.capitalize(this['banco'])
  }

  this._calculate()
}

Boleto.barcodeRenderEngine = 'img'

Boleto.prototype._calculate = function () {
  this['codigo_banco'] = this.bank.options.codigo + '-' + formatters.mod11(this.bank.options.codigo);
  if (!this['nosso_numero_dv']) {
    this['nosso_numero_dv'] = formatters.mod11(this['nosso_numero'].toString())
  }
  this['barcode_data'] = this.bank.barcodeData(this)
  this['linha_digitavel'] = this.bank.linhaDigitavel(this['barcode_data'])
}

Boleto.prototype.renderHTML = function (callback) {
  var self = this

  var renderOptions = self.bank.options
  renderOptions.boleto = self

  // Copy renderHelper's methods to renderOptions
  for (var key in formatters) {
    renderOptions[key] = formatters[key]
  }

  renderOptions['barcode_render_engine'] = Boleto.barcodeRenderEngine
  renderOptions['barcode_height'] = '50'

  if (Boleto.barcodeRenderEngine == 'bmp') {
    renderOptions['barcode_data'] = barcode.bmpLineForBarcodeData(self['barcode_data'])
  } else if (Boleto.barcodeRenderEngine == 'img') {
    renderOptions['barcode_data'] = barcode.binaryRepresentationForBarcodeData(self['barcode_data'])
  }

  renderOptions['boleto']['linha_digitavel_hash'] = hashString(renderOptions['boleto']['linha_digitavel']).toString()

  ejs.renderFile(path.join(__dirname, '/../assets/layout.ejs'), renderOptions, {
    cache: true
  }, function (err, html) {
    if (err) {
      throw new Error(err)
    }

    callback(html)
  })
}

module.exports = function (_banks) {
  banks = _banks
  return Boleto
}
